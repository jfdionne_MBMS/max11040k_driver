#include <linux/spi/spi.h>
#include <linux/mutex.h>
#include <linux/gpio.h>
#include <linux/wait.h>

#include <linux/iio/trigger.h>
#include <linux/iio/iio.h>
#include <linux/iio/sysfs.h>
#include <linux/iio/trigger_consumer.h>
#include <linux/iio/buffer.h>
#include <linux/iio/triggered_buffer.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/completion.h>

#define MAX11040K_VERSION "1.0"
#define MAX11040K_MODNAME "max11040k"

#define MXS_GPIO_NR(bank, nr)	((bank) * 32 + (nr))

int max11040k_gpio_spi_intr_cfg(void)
{
	return irq_set_irq_type(gpio_to_irq(MXS_GPIO_NR(3, 10)),  IRQ_TYPE_EDGE_FALLING);
}

int max11040k_gpio_spi_intr_get_irq(void)
{
	return gpio_to_irq(MXS_GPIO_NR(3, 10));
}

/*--------------------------------------------------------------------*
 *   Modules parameters
 *--------------------------------------------------------------------*/

#define MAX11040KSPI_CLK_SPEED 10000000

/*--------------------------------------------------------------------*
 *   SPI bus id parameter
 *--------------------------------------------------------------------*/
#define MAX11040K_BUS_MODE SPI_CPOL

#define SAMPLING_INSTANT_REG 0x40
#define DATA_RATE_REG 0x50
#define CONFIG_REG 0x60
#define DATA_REG 0x70
#define READ_REG 0x80

#define TX_BUFFER_SIZE 5
#define RX_BUFFER_SIZE 12

struct max11040k_chip_info
{
	const struct iio_chan_spec *channels;
	unsigned int num_channels;
	const struct iio_info *iio_info;
};

struct max11040k_state {
	struct spi_device 		*spidev;
	const struct max11040k_chip_info	*chip_info;
	struct iio_trigger		*trig;
	struct mutex			buf_lock;
	bool					done;
	u8					*tx;
	u8					*rx;
	unsigned int 			irq;
	uint32_t 				clkspeed;
	wait_queue_head_t		wq_data_avail;
};

static int max11040k_spi_write_reg_8(struct iio_dev *idev,
		u8 reg_address,
		u8 val)
{
	int ret;
	struct spi_message msg;
	struct max11040k_state *st = iio_priv(idev);
	struct spi_transfer transfer;
	
	memset(&transfer, 0, sizeof(transfer));
	spi_message_init(&msg);
	
	transfer.tx_buf = st->tx;
	transfer.len = 2;

	mutex_lock(&st->buf_lock);
	st->tx[0] = reg_address;
	st->tx[1] = val;
	
	spi_message_add_tail(&transfer, &msg);
	ret = spi_sync(st->spidev, &msg);
	mutex_unlock(&st->buf_lock);

	return ret;
}

static int max11040k_spi_write_reg_16(struct iio_dev *idev,
		u8 reg_address,
		u16 val)
{
	int ret;
	struct max11040k_state *st = iio_priv(idev);
	struct spi_message msg;
	struct spi_transfer transfer;
	
	memset(&transfer, 0, sizeof(transfer));
	spi_message_init(&msg);
	
	transfer.tx_buf = st->tx;
	transfer.len = 3;

	mutex_lock(&st->buf_lock);
	st->tx[0] = reg_address;
	st->tx[1] = ((val & 0xFF00) >> 8);
	st->tx[2] = (val & 0x00FF);
	spi_message_add_tail(&transfer, &msg);
	ret = spi_sync(st->spidev, &msg);
	mutex_unlock(&st->buf_lock);

	return ret;
}

static int max11040k_spi_write_reg_32(struct iio_dev *idev,
		u8 reg_address,
		u32 val)
{
	int ret;
	struct max11040k_state *st = iio_priv(idev);
	struct spi_message msg;
	struct spi_transfer transfer;
	
	memset(&transfer, 0, sizeof(transfer));
	spi_message_init(&msg);
	
	transfer.tx_buf = st->tx;
	transfer.len = 5;

	mutex_lock(&st->buf_lock);
	st->tx[0] = reg_address;
	st->tx[1] = ((val & 0xFF000000) >> 24);
	st->tx[2] = ((val & 0x00FF0000) >> 16);
	st->tx[3] = ((val & 0x0000FF00) >> 8);
	st->tx[4] = (val & 0x000000FF);
	
	spi_message_init(&msg);
	spi_message_add_tail(&transfer, &msg);
	ret = spi_sync(st->spidev, &msg);
	mutex_unlock(&st->buf_lock);

	return ret;
}

static int max11040k_spi_read_reg_8(struct iio_dev *idev,
		u8 reg_address,
		u8 *val)
{
	int ret;
	struct max11040k_state *st = iio_priv(idev);
	struct spi_message msg;
	struct spi_transfer transfer[2];
	
	memset(&transfer, 0, sizeof(transfer));
	spi_message_init(&msg);
	
	transfer[0].tx_buf = st->tx;
	transfer[0].len = 1;
	transfer[1].rx_buf = st->rx;
	transfer[1].len = 1;

	mutex_lock(&st->buf_lock);
	st->tx[0] = reg_address | READ_REG;
	
	spi_message_add_tail(&transfer[0], &msg);
	spi_message_add_tail(&transfer[1], &msg);
	ret = spi_sync(st->spidev, &msg);
	
	if (ret) {
		dev_err(&st->spidev->dev,
			"problem when reading 8 bit register 0x%02X",
			reg_address);
		goto error_ret;
	}
	*val = st->rx[0];

error_ret:
	mutex_unlock(&st->buf_lock);
	return ret;
}

static int max11040k_spi_read_reg_16(struct iio_dev *idev,
		u8 reg_address,
		u16 *val)
{
	int ret;
	struct max11040k_state *st = iio_priv(idev);
	struct spi_message msg;
	struct spi_transfer transfer[2];
	
	memset(&transfer, 0, sizeof(transfer));
	spi_message_init(&msg);
	
	transfer[0].tx_buf = st->tx;
	transfer[0].len = 1;
	transfer[1].rx_buf = st->rx;
	transfer[1].len = 2;

	mutex_lock(&st->buf_lock);
	st->tx[0] = reg_address | READ_REG;
	
	spi_message_add_tail(&transfer[0], &msg);
	spi_message_add_tail(&transfer[1], &msg);
	ret = spi_sync(st->spidev, &msg);
	
	if (ret) {
		dev_err(&st->spidev->dev,
			"problem when reading 16 bit register 0x%02X",
			reg_address);
		goto error_ret;
	}
	*val = (st->rx[0] << 8) | st->rx[1];

error_ret:
	mutex_unlock(&st->buf_lock);
	return ret;
}

static int max11040k_spi_read_reg_32(struct iio_dev *idev,
		u8 reg_address,
		u32 *val)
{
	int ret;
	struct max11040k_state *st = iio_priv(idev);
	struct spi_message msg;
	struct spi_transfer transfer[2];
	
	memset(&transfer, 0, sizeof(transfer));
	spi_message_init(&msg);
	
	transfer[0].tx_buf = st->tx;
	transfer[0].len = 1;
	transfer[1].rx_buf = st->rx;
	transfer[1].len = 4;

	mutex_lock(&st->buf_lock);
	st->tx[0] = reg_address | READ_REG;
	
	spi_message_add_tail(&transfer[0], &msg);
	spi_message_add_tail(&transfer[1], &msg);
	ret = spi_sync(st->spidev, &msg);
	
	if (ret) {
		dev_err(&st->spidev->dev,
			"problem when reading 32 bit register 0x%02X",
			reg_address);
		goto error_ret;
	}
	*val = ((st->rx[0] << 24) | (st->rx[1] << 16) | (st->rx[2] << 8) | st->rx[3]);

error_ret:
	mutex_unlock(&st->buf_lock);
	return ret;
}

static ssize_t max11040k_readConfiguration(struct device *dev,
		struct device_attribute *attr,
		char *buf)
{
	int ret;
	u8 val = 0;
	struct iio_dev *indio_dev = dev_to_iio_dev(dev);
	struct iio_dev_attr *this_attr = to_iio_dev_attr(attr);

	ret = max11040k_spi_read_reg_8(indio_dev, this_attr->address, (u8 *)&val);
	if (ret)
		return ret;

	return sprintf(buf, "%d\n", val);
}

static ssize_t max11040k_readDataRate(struct device *dev,
		struct device_attribute *attr,
		char *buf)
{
	int ret;
	u16 val = 0;
	struct iio_dev *indio_dev = dev_to_iio_dev(dev);
	struct iio_dev_attr *this_attr = to_iio_dev_attr(attr);

	ret = max11040k_spi_read_reg_16(indio_dev, this_attr->address, (u16 *)&val);
	if (ret)
		return ret;

	return sprintf(buf, "%d\n", val);
}

static ssize_t max11040k_readSamplingInstant(struct device *dev,
		struct device_attribute *attr,
		char *buf)
{
	int ret;
	u32 val = 0;
	struct iio_dev *indio_dev = dev_to_iio_dev(dev);
	struct iio_dev_attr *this_attr = to_iio_dev_attr(attr);

	ret = max11040k_spi_read_reg_32(indio_dev, this_attr->address, (u32 *)&val);
	if (ret)
		return ret;

	return sprintf(buf, "%d\n", val);
}

static ssize_t max11040k_writeConfiguration(struct device *dev,
		struct device_attribute *attr,
		const char *buf,
		size_t len)
{
	struct iio_dev *indio_dev = dev_to_iio_dev(dev);
	struct iio_dev_attr *this_attr = to_iio_dev_attr(attr);
	int ret;
	long val;

	ret = strict_strtol(buf, 10, &val);
	if (ret)
		goto error_ret;
	ret = max11040k_spi_write_reg_8(indio_dev, this_attr->address, val);

error_ret:
	return ret ? ret : len;
}

static ssize_t max11040k_writeDataRate(struct device *dev,
		struct device_attribute *attr,
		const char *buf,
		size_t len)
{
	struct iio_dev *indio_dev = dev_to_iio_dev(dev);
	struct iio_dev_attr *this_attr = to_iio_dev_attr(attr);
	int ret;
	long val;

	ret = strict_strtol(buf, 10, &val);
	if (ret)
		goto error_ret;
	ret = max11040k_spi_write_reg_16(indio_dev, this_attr->address, val);

error_ret:
	return ret ? ret : len;
}

static ssize_t max11040k_writeSamplingInstant(struct device *dev,
		struct device_attribute *attr,
		const char *buf,
		size_t len)
{
	struct iio_dev *indio_dev = dev_to_iio_dev(dev);
	struct iio_dev_attr *this_attr = to_iio_dev_attr(attr);
	int ret;
	long val;

	ret = strict_strtol(buf, 10, &val);
	if (ret)
		goto error_ret;
	ret = max11040k_spi_write_reg_32(indio_dev, this_attr->address, val);

error_ret:
	return ret ? ret : len;
}
static IIO_DEVICE_ATTR(sampling_instant, S_IWUGO | S_IRUGO, max11040k_readSamplingInstant, max11040k_writeSamplingInstant, SAMPLING_INSTANT_REG);
static IIO_DEVICE_ATTR(data_rate, S_IWUGO | S_IRUGO, max11040k_readDataRate, max11040k_writeDataRate, DATA_RATE_REG);
static IIO_DEVICE_ATTR(configuration, S_IWUGO | S_IRUGO, max11040k_readConfiguration, max11040k_writeConfiguration, CONFIG_REG);

static struct attribute *max11040k_device_attrs[] = {
	&iio_dev_attr_data_rate.dev_attr.attr,
	&iio_dev_attr_sampling_instant.dev_attr.attr,
	&iio_dev_attr_configuration.dev_attr.attr,
	NULL
};

static struct attribute_group max11040k_attribute_group = {
	.attrs = max11040k_device_attrs,
};

static const struct iio_info max11040k_info = {
	.attrs = &max11040k_attribute_group,
	.driver_module = THIS_MODULE,
};

static const struct iio_chan_spec max11040k_channel = { 
	.type = IIO_VOLTAGE,
	.channel = 0,
	.scan_type.sign = 's',
	.scan_type.realbits = 96,
	.scan_type.storagebits = 72,
};

static const struct max11040k_chip_info chip_info = {
	.channels = &max11040k_channel,
	.num_channels = 1,
	.iio_info = &max11040k_info,
};

static int max11040k_read_data(struct iio_dev *idev)
{
	struct max11040k_state *st = iio_priv(idev);
	struct spi_message msg;
	struct spi_transfer transfer[2];
	
	memset(&transfer, 0, sizeof(transfer));
	spi_message_init(&msg);
	
	transfer[0].tx_buf = st->tx;
	transfer[0].len = 1;
	transfer[1].rx_buf = st->rx;
	transfer[1].len = 12;
	
	mutex_lock(&st->buf_lock);
	spi_message_add_tail(&transfer[0], &msg);
	spi_message_add_tail(&transfer[1], &msg);
	spi_sync(st->spidev, &msg);
	mutex_unlock(&st->buf_lock);
	return 0;
}

static irqreturn_t max11040k_trigger_handler(int irq, void *p)
{
	struct iio_poll_func *pf = p;
	struct iio_dev *idev = pf->indio_dev;
	struct max11040k_state *st = iio_priv(idev);

	max11040k_read_data(idev);

	iio_push_to_buffers(idev, (u8 *)&st->rx[0]);

	iio_trigger_notify_done(idev->trig);

	return IRQ_HANDLED;
}

static irqreturn_t max11040k_eoc_trigger(int irq, void *private)
{
	struct iio_dev *idev = private;
	struct max11040k_state *st = iio_priv(idev);

	if (iio_buffer_enabled(idev)) {
		disable_irq_nosync(irq);
		iio_trigger_poll(idev->trig, iio_get_time_ns());
	} else {
		max11040k_read_data(idev);
		wake_up_interruptible(&st->wq_data_avail);
	}

	return IRQ_HANDLED;
}


static int max11040k_data_rdy_trigger_set_state(struct iio_trigger *trig,
						bool state)
{
	struct iio_dev *idev = iio_trigger_get_drvdata(trig);
	struct max11040k_state *st = iio_priv(idev);
	int ret = 0;
	
	printk(KERN_INFO "Set trigger state %d\n", state);
	
	if(state == true)
	{
		enable_irq(st->irq);
		
		memset(st->tx, 0, 5);
		st->tx[0] = DATA_REG | READ_REG;
		
		//Read one sample to start everything!!!
		max11040k_read_data(idev);
	}
	else {
		disable_irq_nosync(st->irq);
	}
	return ret;
}

// Very important function that reenable interrupt after each sample
static int max11040k_trig_try_reen(struct iio_trigger *trig)
{
	struct iio_dev *idev = iio_trigger_get_drvdata(trig);
	struct max11040k_state *st = iio_priv(idev);
	enable_irq(st->irq);
	return 0;
}

static const struct iio_trigger_ops max11040k_trigger_ops = {
	.owner = THIS_MODULE,
	.set_trigger_state = &max11040k_data_rdy_trigger_set_state,
	.try_reenable = &max11040k_trig_try_reen,
};

static int max11040k_trigger_init(struct iio_dev *indio_dev)
{
	int ret;
	struct max11040k_state *st = iio_priv(indio_dev);

	st->trig = kmalloc(sizeof(st->trig),  GFP_KERNEL);

	if (st->trig == NULL)
		return -ENOMEM;
	
	st->trig = iio_trigger_alloc("%s-dev%d",  indio_dev->name,  indio_dev->id);

	st->trig->dev.parent = indio_dev->dev.parent;
	iio_trigger_set_drvdata(st->trig,  indio_dev);
	st->trig->ops = &max11040k_trigger_ops;

	ret = iio_trigger_register(st->trig);
	if (ret)
	{
		iio_trigger_unregister(st->trig);
		goto error_free_trig;
	}
	
	return 0;

error_free_trig:
	iio_trigger_free(st->trig);
	return ret;
}

static void max11040k_trigger_remove(struct iio_dev *indio_dev)
{
	struct max11040k_state *st = iio_priv(indio_dev);

	iio_trigger_unregister(st->trig);
	iio_trigger_free(st->trig);
}

static int max11040k_buffer_preenable(struct iio_dev *indio_dev)
{
	struct max11040k_state *st = iio_priv(indio_dev);
	indio_dev->buffer->access->set_bytes_per_datum(indio_dev->buffer, 12);
}

static const struct iio_buffer_setup_ops max11040k_buffer_setup_ops = {
	.preenable = &max11040k_buffer_preenable,
	.postenable = &iio_triggered_buffer_postenable,
	.predisable = &iio_triggered_buffer_predisable,
};

static int  max11040k_buffer_init(struct iio_dev *indio_dev)
{
	return iio_triggered_buffer_setup(indio_dev,  &iio_pollfunc_store_time,
		&max11040k_trigger_handler,  &max11040k_buffer_setup_ops);
}

static void max11040k_buffer_remove(struct iio_dev *indio_dev)
{
	iio_triggered_buffer_cleanup(indio_dev);
}

static int max11040k_probe(struct spi_device *spi) 
{
	int ret = 0;
	struct max11040k_state *st;
	struct iio_dev *indio_dev;
	
	/* Driver state struct allocation and configuration */
	indio_dev = iio_device_alloc(sizeof(*st));
	if (indio_dev == NULL) 
	{
		return -ENOMEM;
	}

	st = iio_priv(indio_dev);
	spi_set_drvdata(spi, indio_dev);
	/* Driver state struct allocation and configuration */
	
	/* SPI init section */
	st->spidev = spi;
	if (!st->spidev) 
	{
		printk(KERN_ERR "max11040k: Unable to get SPI device\n");
		return -ENODEV;
	}

	st->spidev->max_speed_hz = MAX11040KSPI_CLK_SPEED;
	st->spidev->mode = MAX11040K_BUS_MODE;
	
	if (spi_setup(st->spidev) < 0) 
	{
		printk(KERN_ERR "max11040k: Unable to setup SPI bus\n");
		return -EFAULT;
	}
	printk(KERN_INFO "max11040k: SPI device created!\n");
	// SPI GPIO init section

	init_waitqueue_head(&st->wq_data_avail);
	mutex_init(&st->buf_lock);

	// SPI reception buffer allocation
	st->rx = kzalloc(sizeof(*st->rx) * RX_BUFFER_SIZE, GFP_KERNEL);
	if (st->rx == NULL) 
	{
		ret = -ENOMEM;
		goto error_free_iio;
	}

	// SPI transmission buffer allocation
	st->tx = kzalloc(sizeof(*st->tx) * TX_BUFFER_SIZE, GFP_KERNEL);
	if (st->tx == NULL) 
	{
		ret = -ENOMEM;
		goto error_free_rx;
	}

	// IIO device configuration
	st->chip_info = &chip_info;
	indio_dev->id = 0;
	indio_dev->dev.parent = &spi->dev;
	indio_dev->name = MAX11040K_MODNAME;
	indio_dev->modes = INDIO_DIRECT_MODE;
	indio_dev->channels = st->chip_info->channels;
	indio_dev->num_channels = st->chip_info->num_channels;
	indio_dev->info = st->chip_info->iio_info;
	
	//Interrupt configuration max11040k_gpio_spi_intr_cfg
	st->irq = max11040k_gpio_spi_intr_get_irq();
	if (max11040k_gpio_spi_intr_cfg() == 0) {
		if (!request_irq(st->irq,  max11040k_eoc_trigger, 0, indio_dev->name, indio_dev)) {
			printk(KERN_INFO "max11040k: Irq request succeed %d\n", st->irq);
		} else {
			printk(KERN_ERR "max11040k: Fail to request irq %d\n", st->irq);
		}
	} else {
		printk(KERN_ERR "max11040k: Fail to configure interrupt signal\n");
	}
	
	//disable irq for now
	disable_irq_nosync(st->irq);
	
	// IIO buffer init
	ret = max11040k_buffer_init(indio_dev);
	if (ret < 0) 
	{
		printk(KERN_ERR "max11040k: Couldn't initialize the buffer.\n");
		goto error_free_tx;
	}

	// IIO trigger init
	ret = max11040k_trigger_init(indio_dev);
	if (ret < 0) 
	{
		printk(KERN_ERR "max11040k: Couldn't setup the triggers.\n");
		goto error_free_tx;
	}
	
	// IIO device registration
	ret = iio_device_register(indio_dev);
	if(ret) 
	{
		goto error_free_tx;
	}
	printk(KERN_INFO "MAX11040K %s driver loaded\n", MAX11040K_VERSION);

	return 0;
error_free_tx:
	free_irq(st->irq, indio_dev);
	kfree(st->tx);
error_free_rx:
	kfree(st->rx);
error_free_iio:
	iio_device_unregister(indio_dev);
	spi_unregister_device(st->spidev);
	return ret;
}

static int max11040k_remove(struct spi_device *spi)
{
	struct iio_dev *indio_dev = spi_get_drvdata(spi);
	struct max11040k_state *st = iio_priv(indio_dev);
	
	/* SPI GPIO delete */
	spi_unregister_device(st->spidev);
	/* SPI GPIO delete */
	
	/* IIO device delete */
	max11040k_trigger_remove(indio_dev);
	max11040k_buffer_remove(indio_dev);
	iio_device_unregister(indio_dev);
	kfree(st->tx);
	kfree(st->rx);
	/* IIO device delete */
	
	// Driver state struct delete
	iio_device_free(indio_dev);
	free_irq(st->irq, indio_dev);
	
	printk(KERN_INFO "MAX11040K %s driver unloaded\n", MAX11040K_VERSION);
	return 0;
}

static struct spi_driver max11040k_spi_driver = {
	.driver.name	= MAX11040K_MODNAME,
	.driver.owner	= THIS_MODULE,
	.probe		= max11040k_probe,
	.remove		= max11040k_remove,
};

static int __init max11040k_init(void) 
{
	int status;
	
	printk(KERN_INFO "Registering SPI device for max11040k driver\n");
	
	status = spi_register_driver(&max11040k_spi_driver);
	
	if(status < 0)
	{
		printk(KERN_ERR "Failed to register SPI driver\n");
	}
	return status;
}
module_init(max11040k_init);

static void __exit max11040k_exit(void) 
{
	spi_unregister_driver(&max11040k_spi_driver);
}
module_exit(max11040k_exit);

MODULE_DESCRIPTION("Maxim MAX11040K ADC SPI Driver");
MODULE_AUTHOR("MBMS Solutions");
MODULE_LICENSE("GPL");
MODULE_VERSION(MAX11040K_VERSION);

